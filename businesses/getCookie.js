'use strict';
var request = require('request');
var _ = require('lodash');
var htmlparser = require("htmlparser2");
var querystring = require('querystring');

var Cookie = function () {

};
Cookie.prototype.getCookie = function (account, url, callback) {
  var _this = this;
  var objLogin = {};
  request.get({
    url: url,
    followRedirect: true
  }, function (err, res, body) {

    objLogin = {
      '__VIEWSTATE': '',
      '__VIEWSTATEGENERATOR': '',
      '__EVENTVALIDATION': '',
      '__EVENTTARGET': '',
      '__EVENTARGUMENT': '',
      'ctl00$ContentPlaceHolder1$txtUsername': account.username,
      'ctl00$ContentPlaceHolder1$txtPassword': account.password,
      'ctl00$ContentPlaceHolder1$btnlogin': 'Đăng nhập',
      'Header1_TabStrip1_SelectedNode': '',
      'Header1_TabStrip1_ScrollData': 0
    };
    var parser = new htmlparser.Parser({
      onopentag: function (name, attribs) {
        if (name === "input") {
          if (attribs.name == '__VIEWSTATE') {
            objLogin['__VIEWSTATE'] = attribs.value.toString();
          }
          if (attribs.name == '__VIEWSTATEGENERATOR') {
            objLogin['__VIEWSTATEGENERATOR'] = attribs.value.toString();
          }
          if (attribs.name == '__EVENTVALIDATION') {
            objLogin['__EVENTVALIDATION'] = attribs.value.toString();
          }
        }
      },
      ontext: function (text) {
      },
      onclosetag: function (tagname) {
      }
    }, {decodeEntities: true});
    parser.write(body);
    parser.end();
    _this.reqPost(url, objLogin, function (_res) {
      var cookie = _.join(_res.headers['set-cookie'], ';');
      callback(cookie);
    })

  });
};
Cookie.prototype.reqPost = function(url, dataForm, cb) {
  dataForm = querystring.stringify(dataForm);
  request({
      url: url,
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: dataForm
    }
    , function (err, res) {
      cb(res);
    });
};
module.exports = Cookie;