'use strict';
var request = require('request');
var querystring = require('querystring');
var htmlparser = require("htmlparser2");
var _ = require('lodash');
var async = require('async');

var Answer = function () {
};
Answer.prototype.get = function (url, cookie, id, cb) {
  var _this = this;
  var data_form = {
    Cart_QuickComment1_CallBackQuickComment_Callback_Param: id
  };
  data_form = querystring.stringify(data_form);
  request({
      url: url,
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": cookie
      },
      body: data_form
    }
    , function (err, res, body) {
      var head = '<\CallbackContent><!\[CDATA\[';
      var foot = ']]><\/CallbackContent>';
      body = body.replace(head,
        '').replace(foot, '');
      _this.getImageAnswer(body, function (imageAnswer) {
       return cb(err,{id: id, image: imageAnswer});
      })
    });
};
Answer.prototype.getImageAnswer = function (answer, cb) {
  var img = '';
  var parser = new htmlparser.Parser({
    onopentag: function (name, attribs) {
      if (name == 'img' && attribs.alt) {
        img = '<\div style="padding:25px"><img border="0" alt="1g.png" src="'+attribs.src+'"></div>';
      }
    },
    ontext: function (text) {
      //do nothing
    },
    onclosetag: function (tagname) {
      //do nothing
    }
  }, {decodeEntities: true});
  parser.write(answer);
  parser.end();
  if(img == '' && answer.indexOf('HD:') > 0){
    var st = answer.indexOf('HD:');
    var en = answer.indexOf('<\BR></font><BR>');
    img = answer.substring(st, en);
  }else if(answer.indexOf('<\DIV style="padding:15px">Đáp án') >= 0){
    var st1 = answer.indexOf('<\DIV style="padding:15px">Đáp án');
    var en1 = answer.indexOf('Chú ý: Làm ơn gửi');
    img = answer.substring(st1, en1);
  }else if(answer.indexOf('Đáp án') >= 0){
    var st2 = answer.indexOf('Đáp án');
    var en2 = answer.indexOf('<\/div></td></tr>');
    img = '1.'+answer.substring(st2, en2);
  }else if(answer.indexOf('padding:15px">') >= 0){
    var st3 = answer.indexOf('padding:15px">');
    var en3 = answer.indexOf('Chú ý: Làm ơn gửi');
    img = '<\div>Lời giải :</div>'+answer.substring(st3-12, en3);
  }
  cb( img|| '#');
};

Answer.prototype.getAnswerId = function (question, callback) {
  var arrID = [];
  var parser = new htmlparser.Parser({
    onopentag: function (name, attribs) {
      if (attribs.href) {
        var id = attribs.href;
        id = id.replace(/[^0-9\.]/g, '');
        arrID.push(id);
        arrID = _.uniq(arrID);
      }
    },
    ontext: function (text) {
      //do nothing
    },
    onclosetag: function (tagname) {
      //do nothing
    }
  }, {decodeEntities: true});
  parser.write(question);
  parser.end();
  callback(arrID);
};

module.exports = Answer;