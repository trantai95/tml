'use strict';
var request = require('request');
var querystring = require('querystring');
var htmlparser = require("htmlparser2");
var _ = require('lodash');
var async = require('async');
var Question = function () {
};
Question.prototype.get = function (url, cookie, cb) {
  var _this = this;
  var requestUrl = '';
  request({
      url: url,
      method: "GET",
      headers: {
        "Cookie": cookie
      }
    }
    , function (err, res) {
      requestUrl = res.request.href;
      if(requestUrl.indexOf('DangKyThi') > 0){
        return cb('Chưa phát hành');
      }
      var data_form = 'Cart_ContentPlaceHolder1_BaiTapLuyenTap1_CallBackBaiTapLuyenTap_Callback_Param=0&Cart_ContentPlaceHolder1_BaiTapLuyenTap1_CallBackBaiTapLuyenTap_Callback_Param=0';
      request({
          url: requestUrl,
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cookie": cookie
          },
          body: data_form
        }
        , function (err, res, body) {
          var head = '<\CallbackContent><!\[CDATA\[';
          var foot = ']]><\/CallbackContent>';
          body = body.replace(head,
            '').replace(foot, '');
          body = '<\html>\r\n<head><style>a {text-decoration: blink !important;color: #000000 !important;}\r\ndiv {background-color: #ffffff !important;display: block !important}\r\ntable {background-color: #ffffff !important;}</style></head><body>' + body + '</body>\r\n</html>';
          if(body.indexOf('phát hành vào ngày') > 0){
            cb('Chưa phát hành');
          }else if(body.indexOf('AnswerSheet1_lbSheet') > 0){
            _this.getEn(url, cookie, function (_err, _body, _requestUrl) {
              cb(_err, _body, _requestUrl);
            });
          }else if(body.indexOf('Answer Sheet') > 0){
            _this.getEn2(url, cookie, function (_err2, _body2, _requestUrl2) {
              cb(_err2, _body2, _requestUrl2);
            });
          }else if(requestUrl.indexOf('BuyCourse') > 0){
            cb('Tài khoản chưa mua khóa học');
          }else{
            cb(err, body, requestUrl);
          }
        });
    });
};
Question.prototype.renderHtml = function (question, answers, cb) {
  var _question = question;
  async.eachSeries(answers, function(answer, next){
    _question = _question.replace('ID:', '');
    _question = _question.replace('Lời giải & Bình luận', '');
    _question = _question.replace('Nhận biết', '');
    _question = _question.replace('Theo dõi', '');
    _question = _question.replace('Lời giải : ', '');
    _question = _question.replace('Thông hiểu', '');
    _question = _question.replace('100px', '');
    for(var i =0; i < 20; i++){
      _question = _question.replace('('+i+')', '');
    }
    _question = _question.replace('>'+answer.id+'</', '>'+answer.image+'</');
    _question = _question.replace('& Bình luận </', ': <\/br>'+answer.image+'</');
    next();
  }, function(){
    // console.log(_question);
    cb(_question);
  });
};
Question.prototype.getEn = function (url, cookie, cb) {
  var requestUrl = '';
  request({
      url: url,
      method: "GET",
      headers: {
        "Cookie": cookie
      }
    }
    , function (err, res) {
      requestUrl = res.request.href;
      var data_form = 'Cart_CallBackLoadContent_Callback_Param=1';
      request({
          url: requestUrl,
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cookie": cookie
          },
          body: data_form
        }
        , function (err, res, body) {
          var head = '<\CallbackContent><!\[CDATA\[';
          var foot = ']]><\/CallbackContent>';
          body = body.replace(head,
            '').replace(foot, '');
          body = '<\html>\r\n<head><style>a {text-decoration: blink !important;color: #000000 !important;}\r\ndiv {background-color: #ffffff !important;display: block !important}\r\ntable {background-color: #ffffff !important;}</style></head><body>' + body + '</body>\r\n</html>';
          cb(err, body, requestUrl);
        });
    });
};
Question.prototype.getEn2 = function (url, cookie, cb) {
  var requestUrl = '';
  request({
      url: url,
      method: "GET",
      headers: {
        "Cookie": cookie
      }
    }
    , function (err, res) {
      requestUrl = res.request.href;
      var data_form = 'Cart_CallBackContent_Callback_Param=3';
      request({
          url: requestUrl,
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cookie": cookie
          },
          body: data_form
        }
        , function (err, res, body) {
          var head = '<\CallbackContent><!\[CDATA\[';
          var foot = ']]><\/CallbackContent>';
          body = body.replace(head,
            '').replace(foot, '');
          body = '<\html>\r\n<head><style>a {text-decoration: blink !important;color: #000000 !important;}\r\ndiv {background-color: #ffffff !important;display: block !important}\r\ntable {background-color: #ffffff !important;}</style></head><body>' + body + '</body>\r\n</html>';
          cb(err, body, requestUrl);
        });
    });
};
module.exports = Question;