var fs = require('fs');
var path = require('path');
var async = require('async');
var csv = require('fast-csv');
var stream = fs.createReadStream(path.resolve("./input/input.csv"));
var Run = require(path.resolve('./main/run'));
var count = 0;
var arrUser = [];
csv
  .fromStream(stream, {headers : true})
  .on("data", function(data){
    if(data.subjects != ''){
      arrUser.push({
        subjects: data.subjects,
        url: [data.url],
        user: data.user,
        pass: data.pass
      });
      count++;
    }else{
      arrUser[count-1].url.push(data.url);
    }
  })
  .on("end", function(){
    async.eachSeries(arrUser, function(item, next){
      async.eachSeries(item.url, function(_item, _next){
        var run = new Run();
        run.start(item.subjects, item.user, item.pass, _item, function () {
          _next();
        });
      }, function(){
        next();
      });
    }, function(){
      console.log("Hoàn thành bài trong file input");
    });
  });