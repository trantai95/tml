var path = require('path');
var _ = require('lodash');
var async = require('async');
var fs = require('fs');
var string = require('string');
var pdf = require('html-pdf');
var options = {format: 'Letter'};
var writeFile = require('write');

var QuestionBus = require(path.resolve('./businesses/getContentQuestion'));
var questionBus = new QuestionBus();
var AnswerBus = require(path.resolve('./businesses/getContentAnswer'));
var answerBus = new AnswerBus();
var BusinessesMain = function () {

};
BusinessesMain.prototype.mainCreateFile = function (subject, url, title, cookie, callback) {
  var arrAnswer = [];
  questionBus.get(url, cookie, function (err, question, requestUrl, code) {
    if (err) {
      console.log(err);
      callback(err);
    } else {
      // console.log(question);
      answerBus.getAnswerId(question, function (arrId) {
        async.eachSeries(arrId, function (id, nextId) {
          answerBus.get(requestUrl, cookie, id, function (err, answer) {
            if (err) {
              console.log(err);
              nextId();
            } else {
              arrAnswer.push(answer);
              nextId();
            }
          });
        }, function () {
          questionBus.renderHtml(question, arrAnswer, function (bodyHtml) {
            var input = 'display:none';
            var output = '';
            var titlePath = title;
            titlePath = string(titlePath).latinise().s;
            titlePath = titlePath.split(' ').join('');
            bodyHtml = bodyHtml.replace(input, output);
            writeFile('./DocumentsHtml/'+subject._subject+'/'+subject.titleUnit+'/'+title+'/'+titlePath+'.html', bodyHtml, function (err) {
              if (err) console.log(err);
            });
            pdf.create(question, options).toFile('./Documents/'+subject._subject+'/'+subject.titleUnit+'/'+title+'/'+titlePath+'_DE.pdf', function (err) {
              if (err) console.log(err);
            });
            pdf.create(bodyHtml, options).toFile('./Documents/'+subject._subject+'/'+subject.titleUnit+'/'+title+'/'+titlePath+'_DA.pdf', function (err) {
              if (err) {
                console.log(err);
                callback();
              }else{
                callback('done');
              }
            });
          });
        });
      });
    }
  });
};
module.exports = BusinessesMain;

