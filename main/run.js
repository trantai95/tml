var path = require('path');
var async = require('async');
var fs = require('fs-extra');
var _ = require('lodash');
var string = require('string');
var request = require('request');
var htmlparser = require("htmlparser2");

var url_login = 'http://moon.vn/ThanhVien1/Login1.aspx';
// var url = 'http://moon.vn/KhoaHoc/DeCuong/1705/712/1';
var Run = function () {
};

Run.prototype.start = function (nameSubject, user, pass, url, cb) {

var CookiaBus = require(path.resolve('./businesses/getCookie'));
var cookiaBus = new CookiaBus();
var MainBus = require(path.resolve('./main/main'));
var mainBus = new MainBus();
var account = {
  username: user,
  password: pass
};
    request({
        url: url,
        method: "GET",
        headers: {        }
      }
      , function (err, res) {
        var link_data = [];
        var href = '', title = '', title_unit ='', subject = {_subject: nameSubject};
        var parser = new htmlparser.Parser({
          onopentag: function (name, attribs) {
            if (name == 'a' && attribs.href && attribs.href.indexOf('/Buy/Confirm/') >= 0) {
              href = 'http://moon.vn'+attribs.href;
            }
            if(href != '' && name == 'b' && !attribs.hasOwnProperty('style')){
              title = 'true';
            }
            if(name == 'title'){
              title_unit = 'true';
            }

          },
          ontext: function (text) {
            if(title != '' && href != ''){
              text = text.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
                link_data.push({title: _.trim(text), href: href});
                href = ''; title = '';
            }
            if(title_unit != ''){
              subject.titleUnit = _.trim(text);
              console.log('chuyên đề: '+subject.titleUnit);
              title_unit = '';
            }
          },
          onclosetag: function (tagname) {
            //do nothing
          }
        }, {decodeEntities: true});
        parser.write(res.body);
        parser.end();
        cookiaBus.getCookie(account, url_login, function (cookie) {
      if(cookie){
        async.eachSeries(link_data, function(aLink, next){
          var titlePath = aLink.title;
          titlePath = string(titlePath).latinise().s;
          titlePath = titlePath.split(' ').join('');
          var filePath = './Documents/'+subject._subject+'/'+subject.titleUnit+'/'+aLink.title+'/'+titlePath+'_DA.pdf';
          fs.pathExists(filePath, function(err, exist){
            if(exist){
              console.log("Đã có file: "+filePath+'link: '+aLink.href);
              next();
            }else {
              mainBus.mainCreateFile(subject, aLink.href, aLink.title, cookie, function (status) {
                if(status){
                  console.log(status+': '+'title: '+aLink.title+'link: '+aLink.href);
                  if(status == 'Chưa phát hành'){
                    return cb();
                  }else {
                    setTimeout(function(){next()},5000);
                  }
                }else{
                  console.log('Có lỗi xảy ra'+': '+'title: '+aLink.title+'link: '+aLink.href);
                  setTimeout(function(){next()},3000);
                }
              })
            }
          });
        }, function(){
          console.log("Hoàn thành: "+ url);
          cb();
        });
        }else{
          console.log('Có lỗi xảy ra');
          cb();
        }
      });
  });


};

module.exports = Run;